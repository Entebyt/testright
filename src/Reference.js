import React, { Component } from 'react';
import './App.css';
import './hover.css'
import { Card, Button, CardTitle, CardText, Col,CardBody,CardGroup,CardImg,CardImgOverlay,Row} from 'reactstrap';
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
  } from 'reactstrap';
export class Reference extends Component {
    constructor(props) {
        super(props);
        this.state = { activeIndex: 0 };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
      }
      onExiting() {
        this.animating = true;
      }
    
      onExited() {
        this.animating = false;
      }
    
      next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
      }
    
      previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
      }
    
      goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
      }
    
  render() {
    const { activeIndex } = this.state;

    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.quote}
        >
          <div style={{height:"35vh",marginTop:"5vmax"}}><p> {item.quote} </p>
          <p>- {item.by}</p></div>
         
        </CarouselItem>
      );
    });

    return (
      <div className="App">
        <div className="heading" style={{background:"white"}}>
        <div className="headtext" >OUR PRESENCE</div></div>
        <Row style={{margin:"auto"}}>
            <Col sm="6">
            <img style={{width:"100%"}} src={'assets/img/world_map.png'} />
            </Col>
            
            <Col sm="6">
            <div style={{fontSize:"3vmax"}}><i className="fa fa-quote-left" aria-hidden="true"></i></div>
            <Carousel
            ride="carousel"
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
        interval={2000}
      >
        <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
        {slides}
        <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
      </Carousel>
            </Col>
            </Row>
         
      </div>

    );
  }
}
const items = [
    {
      quote: "It's a handy spectrophotometer for any lab with limited space.",
      by: 'Dr. Shashank Deep, Prof. at IIT Delhi',
     
    },
    {
      quote: "We use Prizm for kinetics and absorption spectra. Affordability and compactness will make Prizm a staple in bench research.",
      by: 'Dr. Vinod Kumar, Delhi University',
     
    },
    {
      quote: "Our students will now get to learn hands-on, thanks to Prizm. Now, we can have one for each student in the lab",
      by: 'Dr. Pramit Chaudhary, Faculty Incharge, UG lab, IIT Delhi',
     
    },
    {
      quote: "I use Prizm to check bacterial density on the go. It sits comfortably on my desk like a calculator.",
      by: 'Sonam, Ph.D. scholar, IIT Delhi',
     
    },
  ];