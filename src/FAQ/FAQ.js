import React, { Component } from 'react';
import {
    Accordion,
    AccordionItem,
    AccordionItemTitle,
    AccordionItemBody,
    AccordionArrow,
} from 'react-accessible-accordion';
import './FAQ.css'
import {Card,CardBody,CardImg,CardTitle,CardGroup,Button,CardHeader} from 'reactstrap';
import {Contact} from '../Contact'
import 'react-accessible-accordion/dist/fancy-example.css';
import 'react-accessible-accordion/dist/minimal-example.css';
import {NavigationBar} from '../Navbar'
export class FAQ extends Component {
  constructor(props) {
    super(props);
  
    this.state = { general: true,
                  purchase:false,
                  shipping:false,
                  show:true  };
  }


  show(view){
      if(view == 1){
   this.setState({general:true});
   this.setState({purchase:false});
   this.setState({shipping:false});
   document.getElementById("card1").style.color ="#12b07d";
   document.getElementById("card2").style.color ="black";
   document.getElementById("card3").style.color ="black";

      }
      else if(view == 2){
        this.setState({purchase:true});
        this.setState({general:false});
        this.setState({shipping:false});
        document.getElementById("card2").style.color ="#12b07d";
        document.getElementById("card1").style.color ="black";
        document.getElementById("card3").style.color ="black";
           }
       else {
        this.setState({shipping:true});
        this.setState({purchase:false});
        this.setState({general:false});
        document.getElementById("card3").style.color ="#12b07d";
        document.getElementById("card2").style.color ="black";
        document.getElementById("card1").style.color ="black";
      }     
  }
  open(){
  
    this.setState({show:!this.state.show})
  }
  render() {
    const slides = general_faq.map((general) => {
        return (
           <AccordionItem 
            key={general.q}>
            <AccordionItemTitle role="button">
            <h3>{general.q}
            </h3>
            </AccordionItemTitle>
            <AccordionItemBody>
                <p>{general.a}</p>
            </AccordionItemBody>
          </AccordionItem>
        );
      });
    return (
      <div>
<div><NavigationBar /></div>
<div>
      <div className={this.state.show ? "fixedbutton":"fixed1"}>
    { this.state.show ?  <div>
      <Button onClick={this.open.bind(this)} class="btn btn-outline-dark" style={{backgroundColor:"#141414",width:150}}>We are online<span style={{color:"#4CAF50"}}>  <i class="fa fa-circle" aria-hidden="true"></i></span></Button>
      </div>:<Card style={{border:"none"}}>
        <CardHeader onClick={this.open.bind(this)} style={{backgroundColor:"#141414",color:"#FFF"}}tag="h5">Priyanka <span style={{color:"#4CAF50"}}>  <i class="fa fa-circle" aria-hidden="true"></i></span><i  style={{marginLeft:"61%"}}onClick={this.open.bind(this)} class="fa fa-times"  aria-hidden="true"></i></CardHeader>
        <CardBody>
 <p style={{textAlign:"center"}}>Please enter your details to continue</p>
<iframe className="frame" frameBorder="0" src="http://testright.in/form2"></iframe>
        </CardBody>
    </Card>
   }
      </div>
      </div>
        <div className="heading_FAQ">Frequently Asked Questions</div>
        <CardGroup>
      <Card id="card1" className="faq_card" onClick={this.show.bind(this,1)}>
   
        <CardBody>
              <div className="icons">
          <i class="fa fa-info" aria-hidden="true"></i>
     </div>
          <CardTitle>General{this.state.general}</CardTitle>
         
        </CardBody>
      </Card>
      <Card id="card2" className="faq_card" onClick={this.show.bind(this,2)}>
        <CardImg/>
        <CardBody>
        <div className="icons">
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
        </div> 
          <CardTitle>Purchase</CardTitle>
          
        </CardBody>
      </Card>
      <Card  id="card3" className="faq_card" onClick={this.show.bind(this,3)}>
        <CardImg />
        <CardBody>
        <div className="icons">
        <i class="fa fa-truck" aria-hidden="true"></i>
        </div> 
          <CardTitle>Shipping</CardTitle>
        
        </CardBody>
      </Card>
    </CardGroup>
       {this.state.general ? <div className="accordion_style">
      <Accordion >
        {slides}
    </Accordion>
       </div> :null }
       {this.state.purchase ? <div className="accordion_style">
      <Accordion >
        <AccordionItem >
            <AccordionItemTitle role="button">

                <h3>Purchase Ques 
                  
                </h3>
            </AccordionItemTitle>
            <AccordionItemBody>
                <p>Purchase releated answers</p>
            </AccordionItemBody>
        </AccordionItem>
    </Accordion>
       </div> :null }
       {this.state.shipping ? <div className="accordion_style">
      <Accordion >
        <AccordionItem >
            <AccordionItemTitle role="button">

                <h3>Shipping Ques
                  
                </h3>
            </AccordionItemTitle>
            <AccordionItemBody>
                <p>Shipping releated answers</p>
            </AccordionItemBody>
        </AccordionItem>

    </Accordion>
       </div> :null }
   <div>
       <Contact />
   </div>
      </div>
    );
  
  }
}
const general_faq=[
{
    q:"1. Does Prizm Plus contain a battery?",
    a:" No, Prizm plus requires no battery to operate, it draws power from your laptop or tablet device.",
},
{
    q:"2. What is the wavelength range for Prizm Plus?",
    a:"Prizm Plus operates in the visible range from 395 - 750nm.",
},
{
    q:"3. Can i connect a printer to Prizm Plus for printing the results?",
    a:"No, Prizm plus cannot connect to a printer but it enables user to save the result in .csv or .jpeg formats that can be easily transferred to a device connected to the printer",
},
{
    q:"4. Does Prizm Plus comes pre calibrated?",
    a:" Yes, Prizm Plus comes pre-calibrated and is ready to use straight out of box.",
},
{
    q:"5. Can I use Prizm Plus for Trubid solutions?",
    a:" Yes, Prizm Plus works perfectly for turbid solutions.",
},
{
    q:"6. How many cuvettes can be used at a time?",
    a:" Prizm plus has a single cell holder design.",
},
{
    q:"7. Is Prizm Plus single beam or double beam?",
    a:" Prizm Plus is a single beam spectrophotometer working in the visible range.",
},
{
    q:"8. How do I know if Prizm Plus is suited for my application?",
    a:"Prizm Plus is being used and has been validated in a number of industries. Please go through tha applications page and specifications sheet to understad the device better, In case of any queries please feel free to reach out to us."
,
},
{
   q:"9. How heavy is Prizm Plus?",
   a:"Prizm Plus is really lightweight weighing just 102gms."
},
{
    q:"10. What amount of sample volume is required for taking the results.",
    a:" Depending on the type of cuvette a minimum of 3/4 of the square 13mm cuvette must be filled with the sample."
 },
 {
    q:"11. What is the warm up time for the device?",
    a:"Prizm Plus needs 15mins for warm up when powered on."
 },
 {
    q:"12. What all is included in the Prizm Plus package?",
    a:"The package includes a Prizm Plus device, 1 USB Cable and a set of 2 cuvettes."
 },
 {
    q:"13. Where can I get the spare parts?",
    a:"Testright provides an after sales support on the devices for 3 years."
 },
 {
    q:"14. What type of cuvettes are compatible with the device?",
    a:"13mm square cuvettes are compatible with Prizm Plus."
 },
];
