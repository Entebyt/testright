import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './App.css';
import './footer.css'
import { Row,Col} from 'reactstrap';
import {NavLink} from 'react-router-dom';
export class Contact extends Component {
  render() {
    return (
      <div className="bg">
            <Row style={{margin:"auto"}}>
              <Col xs="2" md="3" className="links_container" >
              <div className="container_test">
            <div className="link_container">
           <div className="links">About &nbsp; &nbsp; &nbsp; Products</div>
           <div className="links">Industries &nbsp; &nbsp; Competition</div>
           <div className="links">FAQ &nbsp; &nbsp; &nbsp; Contact</div>
           </div>

          </div>
              </Col>
      <Col sm="12" md="6">
      <div className="CONTACT_US">CONTACT US</div>
      
      <Row style={{margin:"auto"}}>
      <Col ><div className="Address">ADDRESS</div>
      <div className="Address_text ">Synergy building, IIT Delhi, Hauz Khas, Delhi, India-110016</div>
      </Col>
      <Col ><div className="Address">PHONE</div>
      <div className="Address_text ">+91-9716608806 <br /> +91-8826660363</div> </Col>
      <Col ><div className="Address">EMAIL</div>
      <div className="Address_text ">info@testright.in</div></Col>
      
      </Row>
      <Row><Col style={{textAlign:"center",marginTop:"2.5vmax"}}>
      <div className="TestRight_Nanosystems_Pvt__Ltd_">TestRight Nanosystems Pvt.Ltd. © 2018 All rights reserved.</div>
      </Col></Row>
      </Col>
      <Col xs="3" md="3">
      <div className="social-icons"
      >
          <i class="fa fa-facebook-f"></i>&nbsp;&nbsp;&nbsp;
              <i class="fa fa-twitter"></i>&nbsp;&nbsp;&nbsp;
              <i class="fa fa-google-plus"></i>&nbsp;&nbsp;&nbsp;
              <i class="fa fa-youtube"></i>&nbsp;&nbsp;&nbsp;
              <i class="fa fa-linkedin"></i>&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
           <Row style={{margin:"auto"}}>
           <Col>
          
           </Col>
           </Row>   
      </Col>

      </Row>

      </div>
    
    );
  }
}
