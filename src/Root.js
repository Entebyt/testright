import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Contact} from './Contact'
import {Reference} from './Reference'
import {Products} from './Products'
import ChatBot from 'react-simple-chatbot';
import {Affilate} from './Affilates/Affilates'
import { Button } from 'reactstrap';
import {Experiment} from './Experiment'
import {NavigationBar} from './Navbar'
import {Application} from './Applications'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons'
import {SimpleForm} from './BotMessenger/botmessengerlayout'
import {PrismPlus} from './PrismPlus/PrismPlus';
import {withRouter} from "react-router-dom";
import * as Scroll from 'react-scroll';
import  {Contact_us} from './Contact_us/contact_us';
import { BrowserRouter as Router, Route,HashRouter,Switch } from "react-router-dom";
import {PrizmInfinity} from './PrizmInfinity/PrizmInfinity'
export class Root extends Component {
  Scroll(view){
    debugger
    if(view == 1){
      Scroll.animateScroll.scrollTo(660)
    }
    else{
      Scroll.animateScroll.scrollTo(1800)
    }
  
  }
  render() {
  
    return (
      // <Router>
      <div>
         <NavigationBar func={this.Scroll.bind(this)} />
        <Experiment />
        <div id={"Products"}>
          <SimpleForm /> 
          </div>
          <div >
          <Reference  />
       </div>
       <div id={"Applications"}><Products /> </div>
         <Affilate /> 
        <Contact />  
         

   {/*    <PrizmInfinity /> 
   <PrismPlus /> */}
  
        </div>
        
    );
  }
}
