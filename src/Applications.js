import React, { Component } from 'react';
import './App.css';
import Media from "react-media";
import './hover.css'
import {NavLink} from 'react-router-dom';
import {SimpleForm} from './BotMessenger/botmessengerlayout'
import { Card, Button, CardTitle, CardText,
   Col,CardBody,CardGroup,CardImg,CardImgOverlay,Row} from 'reactstrap';
export class Application extends Component {
  constructor(){
    super();
    this.state={
      hover:false
    };
  }
  src = () => (this.setState({hover:true}))
  src2 = () => (this.setState({hover:true}))
  render() {
  
    return (
      <div>

      <CardGroup style={{border:"none"}}>
      <Card style={{border:"none",borderRadious:0,paddingBottom:13,margin:0,background:"#fbfefc"}}>
        
<div className="top-center">
 <h1 className="Prism_head">PRIZM<sup>+</sup></h1>
 </div>
 <div style={{textAlign:"center"}}>
 <Media query="(max-width: 768px)">
          {matches =>
            matches ? (
              <CardImg  top style={{width:"50%" }}src= "assets/img/pocket-spectrophtotometetr-testright-prizm-plus.png"/>
              // <p>The document is less than 600px wide.</p>
            ) : (
              <CardImg  top style={{width:"50%" }}src= "assets/img/pocket-spectrophtotometetr-testright-prizm-plus.png"/>
            )
          }
          </Media>
    </div>
 <div className="centers">
 <div className="spectro_text" style={{marginBottom:"1vw",color:"#212121"}}>
 POCKET SPECTROPHOTOMETER</div>
 <div className="spectro_price"style={{fontSize:18,paddingBottom:7}}>
 STARTING FROM <span style={{color:"#52ce90",fontSize:24}}>Rs. 59,999</span></div>
  </div>
  <Row style={{margin:"auto",textAlign:"center"}}>
  <Col sm="12" md="6" style={{paddingTop:13,paddingBottom:13}}>
  <NavLink style={{textDecoration:"none"}} to="/Prizm_plus">
  <Button style={{backgroundColor:"transparent",color:"dark",width:180,borderWidth:3,borderRadius:15}}>
    <div style={{color:"#151515",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold",textAlign:"center",background:"transparent"}}>EXPLORE MORE</div>
    </Button>{' '}
    </NavLink>  
    </Col>
  <Col sm="12" md="6" style={{paddingTop:13,paddingBottom:13}}>
  
  <Button onClick={this.props.func} style={{backgroundColor:"#52ce90",color:"dark",width:180,borderWidth:3,borderRadius:15}} 
  ><div style={{color:"white",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>BUY NOW</div></Button>{' '}
    
  </Col></Row>
      </Card>
      <Card style={{border:"none",backgroundColor:"black",paddingBottom:13,margin:0}}>
      
<div className="top-center">
 <h1 className="Prism_head"> PRIZM <sup>∞</sup></h1>
 </div>
 <div style={{textAlign:"center"}}>
 <Media query="(max-width: 768px)">
          {matches =>
            matches ? (
              <CardImg top style={{width:"70%"}}src= "assets/img/touchscreen-spectrophotometer-tesright-prizm-infinity.png"  />
              // <p>The document is less than 600px wide.</p>
            ) : (
              <CardImg top style={{width:"70%"}}src= "assets/img/touchscreen-spectrophotometer-tesright-prizm-infinity.png"  />
            )
          }
          </Media>
    </div>
 <div className="centers">
 <h6 className="spectro_text" style={{color:"white",marginBottom:"1vw"}}>WITH BUILT IN 7" TOUCHSCREEN</h6>
 <div className="spectro_price"style={{color:"white",fontSize:18}}>STARTING FROM <span style={{color:"#52ce90",fontSize:24}}>Rs. 79,999</span></div>
 </div>
   <Row style={{margin:"auto",textAlign:"center"}}>
  <Col sm="12" md="6" style={{paddingTop:13,paddingBottom:13}}>
  <NavLink style={{textDecoration:"none",lineHeight:0}} to="/Prizm_infinity">
  <Button 
   style={{backgroundColor:"transparent",color:"dark",width:180,borderWidth:3,borderRadius:15}}>
    <div style={{color:"white",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>EXPLORE MORE</div>
    </Button>{' '}
    </NavLink>
    </Col>
  <Col sm="12" md="6" style={{paddingTop:13,paddingBottom:13}}>
  <Button onClick={this.props.func} 
  style={{backgroundColor:"#52ce90",color:"dark",width:180,borderWidth:3,borderRadius:15}} 
  ><div style={{color:"white",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>BUY NOW</div></Button></Col></Row>
      </Card>
      </CardGroup>
     
      </div>
    );
  }
}

