import React, { Component } from 'react';
import {
  Card, CardBody, CardGroup, Form, FormGroup, Label, Input, Button, CardHeader
} from 'reactstrap';
import  '../BotMessenger/Bot.css' 
import './contact_us.css'
import {Contact} from '../Contact'
import { NavigationBar } from '../Navbar';
export class Contact_us extends Component {
  constructor(props){
    super(props);
    this.state={
      show:true,
      submit:true,
      message:'',
      interest:'PURCHASE',
      name:'',
      email:'',
      phone:'',
      organization:''
    };
   
  }

open(){
  
  this.setState({show:!this.state.show})
}
  handleSubmit(event) {
    debugger
    var obj=
    { 'form_name':this.state.name,
      'form_email':this.state.email,
      'form_message':
      {
      phone:this.state.phone,
      organization:this.state.organization,
      interest:this.state.interest,
      message:this.state.message
      }
    }

    event.preventDefault();
    
    
    fetch('http://testright.in/mailer', {
      method: 'POST',
      body:{ 'form_name':'123',
      'form_email':'this.state.email',
      'form_message':'this.state.message' }
    }).then(function(res) {
      return res.json()
  });
    
    }
  render() {
    return (
      <div style={{ backgroundColor: "#212121"}}>
      <div style={{background:"#fafafa"}}>
      <NavigationBar />
      </div>
      <div>
      <div className={this.state.show ? "fixedbutton":"fixed1"}>
    { this.state.show ?  <div>
      <Button onClick={this.open.bind(this)} class="btn btn-outline-dark" style={{backgroundColor:"#141414",width:150}}>We are online<span style={{color:"#4CAF50"}}>  <i class="fa fa-circle" aria-hidden="true"></i></span></Button>
      </div>:<Card style={{border:"none"}}>
        <CardHeader onClick={this.open.bind(this)} style={{backgroundColor:"#141414",color:"#FFF"}}tag="h5">Priyanka <span style={{color:"#4CAF50"}}>  <i class="fa fa-circle" aria-hidden="true"></i></span><i  style={{marginLeft:"61%"}}onClick={this.open.bind(this)} class="fa fa-times"  aria-hidden="true"></i></CardHeader>
        <CardBody>
 <p style={{textAlign:"center"}}>Please enter your details to continue</p>
<iframe className="frame" frameBorder="0" src="http://testright.in/form2"></iframe>
        </CardBody>
    </Card>
   }
      </div>
      </div>
      <div >
        <CardGroup style={{ backgroundColor: "black" }}>
          <Card style={{ border: "none", backgroundColor: "black" }}>
            <CardBody>
              <div className="left_side">
                <h1 className="contactus white" >CONTACT US </h1>
                <div className="contactus_text white">
                  <p>We are here for you! Feel free to get in touch with us anytime.</p>
                </div>
                <div >
                  <br />
                  <h4  ><span style={{ color: "#52ce90" }}><i class="fa fa-map-marker" aria-hidden="true"></i> ADDRESS</span></h4>
                </div>
                <div className="mid-left">
                  <h7 className="address" style={{ color: "white" }}>Synergy building, IIT Delhi,<br /> Hauz Khas, Delhi, India-110016</h7>
                </div>
                <div className="mid-left">
                  <br />
                  <h4 className="email_title" ><span style={{ color: "#52ce90" }}>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i> E-MAIL</span></h4>
                </div>
                <div className="white">
                  <h7  > info@testright.in</h7>
                </div>
                <div >
                  <br />
                  <h4 ><span style={{ color: "#52ce90" }}><i class="fa fa-phone" aria-hidden="true"></i>  PHONE</span></h4>
                </div>
                <div >
                  <h7 style={{ color: "white" }}>+91-8826660363 <br />+91-9716608806</h7>
                </div>
              </div>
            </CardBody>
          </Card>

          <Card style={{ border: "none", marginTop: "1vmax", marginBottom: "1vmax",background:"#fafafa" }}>

            <div className="right_side">
              <h2 className="queries">ANY QUERIES?</h2>

              <div >
                <div>Here's how you can reach out to us:</div>
                <div><br /></div>
                <div>a) At our present address. </div>
                <div>b) Send us an email. </div>
                <div>c) Call us. </div>
                <div style={{cursor:"pointer"}} onClick={this.open.bind(this)}>d) <a style={{textDecoration:"underline"}}>Click here to fill the form .</a> </div>

              </div>
            </div>
            <CardBody style={{ marginTop: "1vw", marginLeft: "20%", marginRight: "20%", marginBottom: "8%" }}>
              {/* <Form  method="POST" action="mail.php" onSubmit={this.handleSubmit.bind(this)}>
                <FormGroup>
                  <Input onChange={(event)=>this.setState({name:event.target.value})}
                  value={this.state.name} name="name" placeholder="Name" />
                </FormGroup>
                <FormGroup>
                  <Input onChange={(event)=>this.setState({email:event.target.value})}
                  value={this.state.email} type="email" name="email" placeholder="Email" />
                </FormGroup>
                <FormGroup>
                  <Input onChange={(event)=>this.setState({phone:event.target.value})}
                  value={this.state.phone} name="phone" placeholder="Phone number" />
                </FormGroup>
                <FormGroup>
                  <Input onChange={(event)=>this.setState({organization:event.target.value})}
                  value={this.state.organization} name="organization" placeholder="Organization" />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleSelectMulti">I'm interested in?</Label>
                  <Input onChange={(event)=>this.setState({interest:event.target.value})}
                  value={this.state.interest} type="select" name="selectMulti" id="exampleSelectMulti">
                    <option>Purchase</option>
                    <option>Collaboration</option>
                    <option>Careers</option>
                    <option>Others</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Input onChange={(event)=>this.setState({message:event.target.value})}
                  value={this.state.message}
                  type="textarea" style={{ height: 100 }} name="message" placeholder="Enter message here.." />
                </FormGroup>
                <FormGroup>
                <Input  type="submit" style={{ backgroundColor: "black",color:"#fafafa" }} block size="lg">Submit</Input>
                </FormGroup>
              </Form>
               */}
            </CardBody>
          </Card>
        </CardGroup>
        <Contact />
      </div>
      </div>
    );
  }
}

