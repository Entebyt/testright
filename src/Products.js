import React, { Component } from 'react';
import './App.css';
import './hover.css'
import { Card, Button, CardTitle, CardText, Col,CardBody,CardGroup,CardImg,CardImgOverlay,Row} from 'reactstrap';
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
  } from 'reactstrap';
import Media from "react-media";
export class Products extends Component {
  constructor(props) {
    super(props);
 
  }


  render() {
    
    return (
      <div><div className="heading"  style={{backgroundColor:"#fbfefc"}}>
      <div className="headtext">APPLICATIONS</div></div>
      
      <CardGroup >
      
      <Card inverse style={{margin:0,border:"none"}}>
      {/* <div className="container" style={{padding:0}}> */}
       <CardImg width="100%" src= {"assets/img/applications/spectrophotometer-testright-application-academics-beaker-top-view-compressed.jpg"} alt="Card image cap" />
       {/* <div className="overlay"> */}
       <CardImgOverlay style={{textAlign:"center"}}>
         <CardTitle className="application_texts" style={{color:"black"}}> ACADEMICS</CardTitle>
         <CardText>
         <div className="bottom-center">
      <Button onClick={this.props.func} style={{backgroundColor:"inherit",color:"black",borderColor:"black",borderWidth:3,borderRadius:15}}>
      <div style={{color:"black",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>READ MORE</div>
      </Button>{' '}</div>
      </CardText>
         
       </CardImgOverlay>
       {/* </div>
       </div> */}
      </Card>
      
      <Card inverse style={{margin:0,border:"none"}}>
       <CardImg width="100%" src= {"assets/img/applications/spectrophotometer-testright-application-diagnostics-finger-prick-compressed.jpg"} alt="Card image cap" />
       <CardImgOverlay style={{textAlign:"center"}}>
         <CardTitle className="application_texts"  style={{color:"black"}}>DIAGNOSTICS</CardTitle>
         <CardText>
         <div className="bottom-center">
         <Button onClick={this.props.func} style={{backgroundColor:"inherit",color:"dark",borderColor:"#212121",borderWidth:3,borderRadius:15}}>
      <div style={{color:"black",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>READ MORE</div>
      </Button>{' '}</div></CardText>
         
       </CardImgOverlay>
      </Card>
      <Card inverse style={{margin:0,border:"none"}}>
       <CardImg width="100%" src= {"assets/img/applications/spectrophotometer-testright-application-soil-testing-small-plant-top-view-compressed.jpg"}
        alt="Card image cap" />
       <CardImgOverlay style={{textAlign:"center"}}>
         <CardTitle className="application_texts" style={{color:"black"}}> SOIL & WATER</CardTitle>
         <CardText>
         <div className="bottom-center">
         <Button 
         onClick={this.props.func}
          style={{backgroundColor:"inherit",color:"black",
          borderColor:"black",borderWidth:3,borderRadius:18}}>
      <div style={{color:"black",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold",color:"dark"}}>READ MORE</div>
      </Button>{' '}
      </div></CardText>
         
       </CardImgOverlay>
      </Card>
      <Card inverse style={{margin:0,border:"none"}}>
       <CardImg width="100%" src= {"assets/img/applications/spectrophotometer-testright-application-food-and-agriculture-wheat-grains-bread-top-view-compressed.jpg"}
        alt="Card image cap" />
       <CardImgOverlay style={{textAlign:"center"}}>
         <CardTitle className="application_texts" style={{color:"black"}}> FOOD & AGRI</CardTitle>
         <CardText>
        
         <div className="bottom-center">
      <Button onClick={this.props.func} style={{backgroundColor:"inherit",color:"dark",borderColor:"#212121",borderWidth:3,borderRadius:15}}>
      <div style={{color:"black",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>READ MORE</div>
      </Button>{' '}</div>
      </CardText>
         
       </CardImgOverlay>
      </Card>
         {/* <Card style={{border:"none",margin:0}}>
       <CardImg  top width="100%" src= {"assets/img/4.jpg"}/>
      <div className="app-head-center" style={{color:"white"}}>ACADEMICS</div>
       <div className="bottom-center">
      <Button onClick={this.props.func} style={{backgroundColor:"inherit",color:"white",borderColor:"#efeeee",borderWidth:3,borderRadius:15}}>
      <div style={{color:"white",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>READ MORE</div>
      </Button>{' '}</div>
      
      </Card> */}
      {/* <Card style={{border:"none",margin:0,borderRadius:0}}>
       <CardImg style={{opacity:0.6}} top width="100%"src= {"assets/img/2.jpg"} />
       <div className="app-head-center">DIAGNOSTICS</div>
       <div className="bottom-center">
      <Button onClick={this.props.func} style={{backgroundColor:"inherit",color:"dark",borderColor:"#212121",borderWidth:3,borderRadius:15}}>
      <div style={{color:"black",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>READ MORE</div>
      </Button>{' '}</div>
      </Card> */}
      {/* <Card style={{border:"none",margin:0,borderRadius:0}}>
       <CardImg  top width="100%" src= {"assets/img/1.jpg"} />
       <div className="app-head-center" style={{color:"white"}}>SOIL &WATER</div>
       <div className="bottom-center">
      <Button onClick={this.props.func} style={{backgroundColor:"inherit",color:"white",borderColor:"#efeeee",borderWidth:3,borderRadius:15}}>
      <div style={{color:"white",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>READ MORE</div>
      </Button>{' '}</div>
      </Card>
      <Card style={{border:"none"}}>
       <CardImg  top width="100%" src= {"assets/img/3.jpg"}/>
       <div className="app-head-center">FOOD&AGRI</div>
       <div className="bottom-center">
      <Button onClick={this.props.func} style={{backgroundColor:"inherit",color:"dark",borderColor:"#212121",borderWidth:3,borderRadius:15}}>
      <div style={{color:"black",fontSize:18,fontFamily:"Helvetica",fontWeight:"bold"}}>READ MORE</div>
      </Button>{' '}</div>
      </Card> */}
      </CardGroup>
      </div>
    );
  }
}

