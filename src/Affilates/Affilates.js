import React, { Component } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,Row,Col
} from 'reactstrap';
import './Affilates.css'
import '../hover.css'
const items = [
  // {
  //   src: 'assets/img/logo14.jpg',
  //   src1: 'assets/img/logo1.jpg',
  //   src2: 'assets/img/logo19.jpg'
  // },
  // {
  //   src: 'assets/img/logo3.png',
  //   src1: 'assets/img/logo16.jpg',
  //   src2: 'assets/img/logo8.png'
  // },
  // {
  //   src: 'assets/img/logo6.png',
  //   src1: 'assets/img/logo9.png',
  //   src2: 'assets/img/logo21.png'
  // },
    {
    src: 'assets/img/logo/logo-9-bio-asia-winners-testright-min.jpg',
    src1: 'assets/img/logo/logo-10-birac-testright-min.png',
    src2:'assets/img/logo/logo-11-delhi-university-testright-min.jpg',
    src3: 'assets/img/logo/logo-12-iit-guwahati-testright-min.jpg',
  }, 
     {
    src: 'assets/img/logo/logo-13-iit-roorkee-testright-min.jpg',
    src1:'assets/img/logo/logo-14-falling-walls-lab-2016-testright-min.png',
    src2: 'assets/img/logo/logo-15-startup-summit-2016-gujrat-testright-min.png',
    src3: 'assets/img/logo/logo-16-vizag-fintech-festival-2018-testright-min.png'
  },
  {
    src:'assets/img/logo/logo-17-india-innovative-initiative-testright-min.png',
    src1: 'assets/img/logo/logo-18-startup-oasis-testright-min.png',
    src2: 'assets/img/logo/logo-19-nirma-university-testright-min.jpg',
    src3:'assets/img/logo/logo-20-startupindia-testright-min.png',
    
  },

];

export class Affilate extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;
   
    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src1}>
          <Row style={{ margin: "auto", marginTop: "20px", marginBottom: "20px" }}>

            <Col style={{ border: "none", textAlign: "center" }}>

              <img src={item.src} className="logo" />

            </Col>
            
            <Col style={{ border: "none", textAlign: "center" }}>
              
              <img src={item.src1} className="logo" />
            
            </Col>
            <Col style={{ border: "none", textAlign: "center" }}>
              
              <img src={item.src2} className="logo" />
            
            </Col>
            <Col style={{ border: "none", textAlign: "center" }}>
              
              <img src={item.src3} className="logo" />
            
            </Col>
          </Row>
        </CarouselItem>
      );
    });
    return (
      <div>
        <div className="heading"  style={{backgroundColor:"#fbfefc"}}>
      <div className="headtext">OUR ASSOCIATES</div>
      </div>
        <Row style={{margin:"auto",marginTop:"20px",marginBottom:"20px"}}>
         <Col style={{border:"none",textAlign:"center"}}>
         <img src= 'assets/img/logo/logo-1-bio-international-convention-top-innovator-boston-testright-min.jpg' className="logo" />
         </Col> 
         <Col style={{border:"none",textAlign:"center"}}>
         <img src='assets/img/logo/logo-2-merkel technologies-israel-testright-min.jpg' className="logo" />
         </Col> 
         <Col style={{border:"none",textAlign:"center"}}>
         <img src='assets/img/logo/logo-3-qualcomm-testright-min.jpg' className="logo" />
         </Col> 
         <Col style={{border:"none",textAlign:"center"}}>
         <img src='assets/img/logo/logo-4-postech-south-korea-testright-min.jpg' className="logo" />
         </Col> 
         </Row>
         <Row style={{margin:"auto",marginTop:"20px",marginBottom:"20px"}}>
          <Col style={{border:"none",textAlign:"center"}}>
          <img src= 'assets/img/logo/logo-5-accelerate-2016-lebanon-testright-min.jpg' className="logo" />
          </Col> 
         <Col style={{border:"none",textAlign:"center"}}>
         <img src='assets/img/logo/logo-6-iit-bombay-testright-min.png' className="logo" />
         </Col> 
         <Col style={{border:"none",textAlign:"center"}}>
         <img src='assets/img/logo/logo-7-iit-delhi-testright-min.png' className="logo" />
         </Col> 
         <Col style={{border:"none",textAlign:"center"}}>
         <img src='assets/img/logo/logo-8-iit-madras-testright-min.jpg' className="logo" />
         </Col> 
         </Row>
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
        interval={1000}
        autoPlay= {true}
        ride="carousel"

      >
        {slides}
        <CarouselControl style={{backgroundColor:"black"}}
         direction="prev" directionText="Previous" onClickHandler={this.previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
      </Carousel>
      </div>
    );
  }
}