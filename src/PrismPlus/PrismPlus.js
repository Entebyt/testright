import React, { Component } from 'react';
import { NavigationBar } from '../Navbar'
import {Contact} from '../Contact'
import Background from '../experiment.jpg'
import  '../BotMessenger/Bot.css'
import {
  Card, Button, CardImg, CardTitle, CardText, CardGroup, Row, Col,
  CardHeader, CardBody,CardSubtitle
} from 'reactstrap';
import '../hover.css'
import Media from "react-media";
export class PrismPlus extends Component {
  constructor(props){
    super(props);
    this.state={
      show:true,
      submit:true
    };
   
  }

open(){
  
  this.setState({show:!this.state.show})
}
  render() {
    return (
      <div>
      <div className="nav_bar" style={{ textAlign: "center" }}>
        <NavigationBar />
        <div className={this.state.show ? "fixedbutton":"fixed1"}>
    { 
      this.state.show ?  <div>
      <Button
       onClick={this.open.bind(this)} class="btn btn-outline-dark" 
      style={{backgroundColor:"#141414",width:150}}>We are online<span style={{color:"#4CAF50"}}>  <i class="fa fa-circle" aria-hidden="true"></i></span></Button>
      </div>
      :
      <Card style={{border:"none"}}>
        
        <CardHeader onClick={this.open.bind(this)} style={{backgroundColor:"#141414",color:"#FFF"}}tag="h5">Priyanka <span style={{color:"#4CAF50"}}> 
         <i class="fa fa-circle" aria-hidden="true"></i></span>
         <i  style={{marginLeft:"61%"}} 
         onClick={this.open.bind(this)} class="fa fa-times"  aria-hidden="true"></i></CardHeader>
        
        <CardBody>

          <p style={{textAlign:"center"}}>Please enter your details to continue</p>
          <iframe className="frame" frameBorder="0" src="http://testright.in/form2"></iframe>
        
        </CardBody>

      </Card>
   }
      </div>
        {/* <div className="back" >

          <Row style={{ margin: "auto" }} >
            <Col style={{marginTop:"10%"}} >
            <img className="Prism_plus_page" src="assets/img/prizm+.png" />
            </Col>
            <Col>
             <div >
            <div className="align-text-bottom product_head prism_plus_main">PRIZM+</div>
            <div className="spectro_text">POCKET SPECTROPHOTOMETER</div>
            </div>
        
            </Col>
          </Row>
          <Row style={{ margin: "auto" }}><Col style={{ textAlign: "left"}}>
         
            </Col><Col>  
            <div style={{textAlign:"right"}}>
          <div className="d-flex" style={{flexDirection:"column",justifyContent:"",alignItems: "center"}}>
           </div>
          </div>
          </Col>
          </Row>
          <Row style={{ margin: "auto" }}>
          <Col>
          </Col>
          <Col style={{textAlign:"center"}}> 
              <div  style={{
                color: "#fafafa", fontSize: "3.0vmax"
              }}> Your hardwork deserves<br /> a better partner! 
              <br /><br />
              <div> <Button onClick={this.props.func} 
          style={{background:"transparent",borderWidth:3,borderColor:"#fafafa",borderRadius:15}}>
            <div className="button_prizm_plus_text">ORDER NOW</div>
          </Button>{' '}</div>
              </div> 
            </Col>
            
             </Row>
        </div> */}
        	{/* <section id="grid" class="grid clearfix">
				<a href="#" data-path-hover="m 180,34.57627 -180,0 L 0,0 180,0 z">
					<figure>
						<img src="assets/img/prizm+.png" />
						<svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 180,160 0,218 0,0 180,0 z"/></svg>
						<figcaption>
							<h2>Crystalline</h2>
							<p>Soko radicchio bunya nuts gram dulse.</p>
							<button>View</button>
						</figcaption>
					</figure>
				</a>
        </section> */}
          <div className="back ">
            <Row style={{margin:"auto auto auto 20px"}}>

              <Col md="6" sm="12" style={{paddingRight:"90px"}}>


                <Media query="(max-width: 844px)">
          {matches =>
            matches ? (
              <div >
              <Row  style={{margin:"20% auto auto auto "}}>
              <div  style={{
              color: "#151515", fontSize: "3.0vmax",textAlign:"right",marginLeft:"auto"
            }}> Your hardwork deserves<br /> a better partner! 
               <br />
            </div>
              </Row>
             </div>
            ) : (
              <div >
                <Row style={{margin:"auto"}}>
                <div className="prism_plus_main" style={{
                color: "#151515", fontSize: "3.0vmax",textAlign:"right",marginLeft:"auto"
              }}> Your hardwork deserves<br /> a better partner! 
                 <br />< br/>
              </div>
                </Row>
               </div>
            )
          }
          </Media>
                <Row style={{margin:"auto"}}>
                <div style={{marginLeft:"auto",}}>
            <div style={{textAlign:"right"}} className="product_head ">PRIZM<sup>+</sup></div>
            <div style={{textAlign:"right"}} className="spectro_text">POCKET SPECTROPHOTOMETER</div>
            </div>
                </Row>

                <Row style={{margin:"auto"}}>
                <div style={{marginLeft:"auto"}}>
                <Media query="(max-width: 844px)">
          {matches =>
            matches ? (
              <div><br /></div>
            ) : (
              <div><br /><br /><br /> </div>
            )
          }
          </Media> 
                <div >
                <Button onClick={this.open.bind(this)}
          style={{background:"transparent",borderWidth:2,borderColor:"#151515",borderRadius:15}}>
            <div className="button_prizm_plus_text">ORDER NOW</div>
          </Button>{' '}</div>
          <Media query="(max-width: 844px)">
          {matches =>
            matches ? (
              <div><br /></div>
            ) : (
              <div><br /><br /><br /></div>
            )
          }
          </Media>
          </div>
                </Row>
                
              </Col>

              <Col md="6" sm="12">
              <Media query="(max-width: 844px)">
          {matches =>
            matches ? (
              <img style={{width:"70%"}}  src="assets/img/pocket-spectrophtotometetr-testright-prizm-plus.png" />
            
            ) : (
              <img className="Prism_plus_page prism_plus_main" src="assets/img/pocket-spectrophtotometetr-testright-prizm-plus.png" />
            )
          }
          </Media>
              
              </Col>
            </Row>
          </div>
        <Row style={{ margin: "auto", backgroundColor: "#555555" }}>
          <Col md="4" sm="12">
            <Card body style={{ backgroundColor: "#555555", border: "none" }}>
              <CardImg top style={{ width: "5vmax", display: "block", marginLeft: "auto", marginRight: "auto",marginBottom:5 }}
                src={"assets/img/prizm_plus_1.png"} />
              <CardTitle className="prizm_des_head" style={{ textAlign: "center", color: "white" }}>COMPACT</CardTitle>
              
              <CardText className="prizm_des" style={{ color: "#c8c8c8" }}>{message.compact}</CardText>

            </Card>
          </Col>
          <Col md="4" sm="12">
            <Card body style={{ backgroundColor: "#555555", border: "none" }}>
              <CardImg top style={{ width: "5vmax", display: "block", marginLeft: "auto", marginRight: "auto",marginBottom:5 }}
                src={"assets/img/prizm_plus_2.png"} />
              <CardTitle className="prizm_des_head" style={{ textAlign: "center", color: "white" }}>AFFORDABLE</CardTitle>
              <CardText className="prizm_des" style={{ color: "#c8c8c8" }} >{message.affordable}</CardText>

            </Card>
          </Col>
          <Col md="4" sm="12">
            <Card body style={{ backgroundColor: "#555555", border: "none" }}   >
              <CardImg top style={{ width: "4.4vmax", display: "block", marginLeft: "auto", marginRight: "auto",marginBottom:5 }}
                src={"assets/img/prizm_plus_3.png"} />
              <CardTitle className="prizm_des_head" style={{ textAlign: "center", color: "white" }}>SPECIFICATIONS</CardTitle>
              <CardText className="prizm_des" style={{ color: "#c8c8c8" }}>{message.specifications}</CardText>
              <div style={{ textAlign: "center" }}>
              <a href="testright.in/TestRight-Nanosystems-Brochure-compressed.pdf">  <Button style={{ backgroundColor: "inherit", color: "white", borderColor: "#efeeee", width: 160, borderWidth: 3, borderRadius: 15 }}>
                  <div style={{ color: "white", fontSize: 12, fontFamily: "Helvetica", fontWeight: "bold", textTransform: "uppercase" }}>Download Pdf</div>
                </Button>{' '}</a></div>
            </Card>
          </Col>
        </Row>
        <div
          style={{
            height: "46vmax",
            backgroundColor: "white",
          }} >
          <div className="prizm_des">
            <CardGroup className="prizm_plus_des_cardgroup">
              <Card body className="prizm_plus_des_card">
                <Row style={{ margin: "0px" }}>
                  <Col md="6" sm="6" xs="6" className="border_1">
                  <div  className="container" >
                  <img className="image_description"
                    src={"assets/img/prizm_plus_4.png"} />
                    
                    <p className="prizm_des_head">FULL SPECTRUM GRAPH</p>
                    
                   <div className="overlay"> <p className="justify_content">{message.fullspectrum}</p> </div>
                  </div></Col>
                  <Col md="6" sm="6" xs="6" className="border_2">
                  <div className="container">
                    <img className="image_description"
                      src={"assets/img/prizm_plus_5.png"} />
                    <p className="prizm_des_head">GRATING BASED</p>
                   
                    <div className="overlay">
                    <p className="justify_content">{message.grating}</p>
                    </div>
                  </div></Col>
                </Row>
                <Row style={{ margin: "0px" }}>
                  <Col md="6" sm="6" xs="6" className="border_1">
                  <div className="container">
                  <img 
                  className="image_description"
                    src={"assets/img/prizm_plus_6.png"} />
                    <p className="prizm_des_head">LIFETIME LAMP WARRANTY</p>
                    
                    <div className="overlay">
                    <p className="justify_content">{message.lamp}</p>
                    </div>
                  </div></Col>
                  <Col md="6" sm="6" xs="6" className="border_2">
                  <div className="container">
                  <img 
                  className="image_description"
                    src={"assets/img/prizm_plus_7.png"} />
                    <p className="prizm_des_head">COMPATIBILITY</p>
                   
                    <div className="overlay">
                    <p className="justify_content">{message.compatibility}</p>
                    </div>
                  </div></Col>
                </Row>
                <Row style={{ margin: "0px" }}>
                  <Col md="6" sm="6" xs="6" style={{border:"thin",borderStyle:"none solid none none",padding:0}}><div className="container"><img 
                  className="image_description" src={"assets/img/prizm_plus_8.png"} />
                    <p className="prizm_des_head">ZERO MAINTENANCE COSTS</p>
                    
                    <div className="overlay">
                    <p className="justify_content">{message.maintenance}</p>
                    </div>
                  </div></Col>
                  <Col md="6" sm="6" xs="6" style={{padding:0}} >
                  <div className="container">
                  <img className="image_description"
                    src={"assets/img/prizm_plus_9.png"} />
                    <p className="prizm_des_head">CSV</p>
                   
                    <div className="overlay">
                    <p className="justify_content">{message.csv}</p>
                    </div>
                  </div></Col>
                </Row>
              </Card>
              <Card body style={{ border: "none",background:"transparent",padding:0 }}>
              <CardTitle style={{textAlign:"center",marginTop:"10px"}}>The most advanced software in the industry</CardTitle>
              <div style={{ textAlign: "center" }}>
                <CardImg top style={{
                  width: "90%", display: "block",padding:"0 20px",textAlign:"center",margin:"auto"
                }} src="assets/img/software.png" />
                
                <CardTitle style={{letterSpacing:40,fontSize:30,marginLeft:40}}>
                <i class="fa fa-apple" aria-hidden="true"></i>
                <i class="fa fa-android" aria-hidden="true"></i>
                <i class="fa fa-windows" aria-hidden="true"></i>
                <i class="fa fa-linux" aria-hidden="true"></i>
              </CardTitle>
                {/* <div className="makesusremarkable" >WHAT MAKES<br /> US REMARKABLE</div>
                <div > <p style={{ textTransform: "uppercase", fontFamily: "Raleway" }}>Every once in a while, a new <br />technology, an old problem, and<br /> a big idea turn into an innovation. </p></div> */}
              </div></Card>
            </CardGroup>
          </div>
          <div className="white">
            <CardGroup style={{ backgroundColor: "#555555" }}>
              <Card style={{ border: "none", margin: 0, backgroundColor: "#555555" }}>
                <CardBody >
                  <CardImg top style={{
                    width: "13vmax", display: "block",
                    marginRight: "auto", marginLeft: "auto"
                  }} src="assets/img/prism_plus_page5.png" />
                  <CardTitle className="makesusremarkable">HOW IT WORKS</CardTitle>
                  <CardText>See Prizm Plus in action.</CardText>

                </CardBody>
              </Card>
              <Card style={{ border: "none", margin: 0, backgroundColor: "#555555" }}>
                <CardBody style={{padding:0}} >
                  <div>
                <iframe style={{height:"50vh"}} height="100%" width="100%" src="https://www.youtube.com/embed/q8drKAm9jGM" frameborder="0" allow="autoplay; encrypted-media" ></iframe>
                </div>
                </CardBody>
              </Card>
            </CardGroup>
            <Contact />
          </div>
        </div>
       
</div>

      </div>
    );
  }
}

const message = {
  compact: "Carry in your shirt's pocket or keep in the laminar hood. Experience the flexibility like never before.",
  affordable: "We believe it's your right to have access to the latest technology and we won't let cost come in its way.",
  specifications: "a) Wave Range: 395-750nm  b) Accuracy: 0.02abs",
  fullspectrum: "Scan entire wavelength graph and analyze peaks with our powerful software. Or you can use Optical Density or Kinetics mode.",
  grating: "Experience the sharp resolution and accuracy of the scans with our in-house produced diffraction grating.",
  lamp: "Lamp replacement days are over. Special-built in LEDs offer higher stability and 100x more life than conventional lamps.",
  compatibility: "Absolute convenience in order to directly plug and play all tests across multiple devices including laptops, and even tablets and smartphones!",
  maintenance: "Don’t you hate frequent breakdowns? Us too. Prizm offers zero moving parts technology which ensures longer calibration life, thus eliminating further costs..",
  csv: "Get all your essential data & results sorted easily with the additional built-in functionality of directly exporting to a .csv file",
  howitworks: "tell me and i forget. teach me and i rememeber. involve me and i learn.",
  remarkable: "Every once in a while, a new technology, an old problem, and a big idea turn into an innovation."
};