import React from 'react';
import * as Scroll from 'react-scroll';
import { HashLink as Link } from 'react-router-hash-link';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';
  import './App.css';
  import {NavLink} from 'react-router-dom';
export class NavigationBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
      ,close:true,
      gif:false,
      img:""
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  gif(){
    // debugger
    this.setState({gif:true});
    var self=this;
    setTimeout(()=>self.setState({gif:false}),2500)
  }
  click(view){
    this.props.func(view);
  }

  render() {
    return (
      <div 
      style={{opacity:"inherit" 
    }} >
        < Navbar style={{zIndex:9999}}  light expand="md">
          <NavbarBrand href="/">
            <img src={'assets/img/logo.png'} style={{width:"20vmax"}} /> 
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} style={{background:"white"}} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem active>
                <NavLink style={{textDecoration:"none"}} to="/"><div className="navbartext">&nbsp;HOME&nbsp;&nbsp;&nbsp;</div></NavLink>
              </NavItem>
              <NavItem >
                <NavLink style={{textDecoration:"none"}} to="/Aboutus"><div className="navbartext">ABOUT&nbsp;&nbsp;&nbsp;</div></NavLink>
              </NavItem>
              <NavItem >
                <Link style={{textDecoration:"none"}} to="/#Products"><div className="navbartext">PRODUCTS&nbsp;&nbsp;&nbsp;</div></Link>
              </NavItem>
              <NavItem >
                <Link style={{textDecoration:"none"}} to="/#Applications"><div className="navbartext">APPLICATIONS&nbsp;&nbsp;&nbsp;</div></Link>
              </NavItem>
              <NavItem >
                <NavLink style={{textDecoration:"none"}} to="/FAQ"><div className="navbartext">FAQ&nbsp;&nbsp;&nbsp;</div></NavLink>
              </NavItem>
              <NavItem >
                <NavLink style={{textDecoration:"none"}} to="/contact"><div className="navbartext">CONTACT&nbsp;&nbsp;&nbsp;</div></NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
    </div>
    );
  }
}