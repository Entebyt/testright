import React, { Component } from 'react';
import {
  Card, CardBody, Button, CardHeader,Row,Col,CardImg,CardTitle,CardSubtitle,CardText
} from 'reactstrap';
import  '../BotMessenger/Bot.css' 
import './AboutUs.css'
import {Contact} from '../Contact'
import { NavigationBar } from '../Navbar';
export class AboutUs extends Component {
  constructor(props){
    super(props);
this.state={
  show:true
}
   
  }

open(){
  
  this.setState({show:!this.state.show})
}

  render() {
    return (
      <div >
      <div style={{background:"#eff2f7"}}>
      <NavigationBar />
      </div>
      <div>
      <div className={this.state.show ? "fixedbutton":"fixed1"}>
    { this.state.show ?  <div>
      <Button onClick={this.open.bind(this)} class="btn btn-outline-dark" style={{backgroundColor:"#141414",width:150}}>We are online<span style={{color:"#4CAF50"}}>  <i class="fa fa-circle" aria-hidden="true"></i></span></Button>
      </div>:<Card style={{border:"none"}}>
        <CardHeader onClick={this.open.bind(this)} style={{backgroundColor:"#141414",color:"#FFF"}}tag="h5">Priyanka <span style={{color:"#4CAF50"}}>  <i class="fa fa-circle" aria-hidden="true"></i></span><i  style={{marginLeft:"61%"}}onClick={this.open.bind(this)} class="fa fa-times"  aria-hidden="true"></i></CardHeader>
        <CardBody>
 <p style={{textAlign:"center"}}>Please enter your details to continue</p>
<iframe className="frame" frameBorder="0" src="http://testright.in/form2"></iframe>
        </CardBody>
    </Card>
   }
      </div>
      </div>
        <div>
        <Row style={{margin:"auto",background:"#eff2f7"}}>
            <Col  md="6" sm="12" xs="12" style={{padding:0}}>
              <Card style={{border:"none",background:"transparent"}}>
                <CardBody>
                  <CardTitle className="About_head">About Testright Nanosystems</CardTitle>
                  
                 <CardText>With a mission of quintessential quality, convenience and efficiency, TestRight Nanosystems has revitalized Spectrometry as a whole.</CardText>
                  <CardSubtitle className="subtitle">WHO WE ARE?</CardSubtitle>
                  <p>We are a small team of highly skilled professionals dedicated to revolutionize spectrophotometers and its perception across various industries all over the world. </p>
                  <CardSubtitle className="subtitle">WHAT WE DO?</CardSubtitle>
                  <p>We have researched and developed high performance and portable Spectrophotometers that one can buy on a budget.  </p>
                </CardBody>
              </Card>
            </Col>
            <Col md="6" sm="12" xs="12"  style={{padding:0}}>

              <Card style={{border:"none",background:"transparent"}}>
                <CardBody>
                <CardImg  width="100%"  src={"assets/img/officevector.png"}/>
                 
                
                </CardBody>
              </Card>
            </Col>
        </Row>
      
        </div>
      <div >
        <Contact />
      </div>
      </div>
    );
  }
}

