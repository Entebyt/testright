import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Contact} from './Contact'
import {Reference} from './Reference'
import ChatBot from 'react-simple-chatbot';
import { Button } from 'reactstrap';
import {Experiment} from './Experiment'
import {NavigationBar} from './Navbar'
import {Application} from './Applications'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons'
import {SimpleForm} from './BotMessenger/botmessengerlayout'
import {PrismPlus} from './PrismPlus/PrismPlus';
import {withRouter} from "react-router-dom";
import  {Contact_us} from './Contact_us/contact_us';
import {PrizmInfinity} from './PrizmInfinity/PrizmInfinity'
import { BrowserRouter as Router, Route,Switch,HashRouter } from "react-router-dom";
import {FAQ} from './FAQ/FAQ'
import{Root} from './Root'
import {AboutUs} from './AboutUs/AboutUs'
import ScrollToTop from 'react-router-scroll-top'
library.add(faStroopwafel)

class App extends Component {
  render() {
    return (
      <div>
      
         

      {/*  <PrizmInfinity /> 
  <PrismPlus /> */}
   <HashRouter onUpdate={() => window.scrollTo(0, 0)}>
     <Switch>
     <ScrollToTop>
      <Route  path="/contact" component={Contact_us} />
      <Route  path='/prizm_plus' component={PrismPlus} />
      <Route  path='/prizm_infinity' component={PrizmInfinity} />
      <Route path='/FAQ' component={FAQ} />
      <Route path='/aboutus' component={AboutUs} />
      <Route exact path="/" component={Root}></Route>
      </ScrollToTop>
      </Switch>
      </HashRouter>
        </div>
        
    );
  }
}

export default App;
