import React, { Component } from 'react';
import './App.css';
import './loading.css'
import Media from "react-media";
export class Experiment extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
      ,close:true,
      gif:false,
      loaded:false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  gif(){
    this.my_image2 = new Image();
    this.my_image2.src="assets/img/gif2.gif";
    this.setState({gif:true});
    
  }
  gif_loaded(){
  this.setState({loaded:true})
  setTimeout(()=>{this.setState({gif:false})},5600);
}
  render() {
    
    // if(window.screen.width<=766 && window.screen.orientation.type == "portrait-primary"){
    //   back="assets/img/experiment2.jpg";
    // }
    // else if(window.screen.orientation.type == "landscape-primary"){
    //   back="assets/img/experiment.jpg"
    // }
    return (
      <div className="App2" >
        <div className='Container'>
        <div className='Desk'>
        <Media query="(max-width: 768px)">
          {matches =>
            matches ? (
              <img  src={"assets/img/experiment2.jpg"} className='Desk-image' />
            ) : (
              <img  src={"assets/img/pocket-spectrophotometer-prizm-showcase-hero-image-testright.jpg"} className='Desk-image' />
            )
          }
        </Media>
         
        </div>

        <div className='Screen'>
          {
            this.state.gif ?
          <div
           style={{zIndex:9999,width:"100%",
           alignItems:"center",justifyContent:"center",
           display:"-webkit-flex"
           ,height:"100%"}}>
          <div style={this.state.loaded ?{display:"none"}:{}} class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
             <img style={this.state.loaded ?{}:{display:"none"}} onLoad={this.gif_loaded.bind(this)} src={this.my_image2.src} className='Screen-image' />
         </div>
      
            :
              <div  className='Main-Button' onClick={this.gif.bind(this)} >
                 <span  class="pulse-button" onClick={this.gif.bind(this)}>ACQUIRE</span>
              </div>
          }
          
        </div>

        


        </div>
      </div>
    );
  }
}

